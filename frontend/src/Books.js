import React, { Component } from 'react';
import client from './feathers';

class Books extends Component {
    constructor(props) {
        super(props);

        this.state = { };
        this.addBook = this.addBook.bind(this);
    }

    componentDidMount() {
    const bookList = client.service('books');
    
    bookList.find({
      query: {
        $limit: 25
      }
    }).then( bookPage => {
      const books = bookPage.data;

      this.setState({ 
          bookList: bookList, 
          books : books
          });
    }).catch(error => {
           console.log(error);
    });;
    
    bookList.on('created', book => this.setState({
      books: this.state.books.concat(book)
    }));
    
    // Remove a book from the list
    bookList.on('removed', id => {
    console.log(id)
      const newbook = this.state.books.filter((ibook,index,arr) => {
        return ibook._id !== id;
      });
      this.setState({ 
        books: newbook
      });
    });
  }

  deleteBook(id, ev) {
    this.state.bookList.remove(id);
  }

    
  addBook(ev) {

    const inputISBN = ev.target.querySelector('[id="isbn"]');
    const isbn = parseInt( inputISBN.value.trim() );

    const inputTitle = ev.target.querySelector('[id="title"]');
    const title = inputTitle.value.trim();

    const inputPages = ev.target.querySelector('[id="pages"]');
    const pages = parseInt( inputPages.value.trim() );

    console.log( "ISBN: " + isbn );
    console.log( "Title: " + title );
    console.log( "Pages: " + pages );
    
    this.state.bookList.create({
        isbn,
        title,
        pages
    })
    .then(() => {
        inputISBN.value = '';
        inputTitle.value = '';
        inputPages.value = '';
    });

    ev.preventDefault();
  }

  render() {
    return(
    <div>
      <div className="py-5 text-center">
        <h2>Books</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addBook.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">ISBN</label>
                <input type="number" className="form-control" id="isbn" defaultValue="1" required />
                <div className="invalid-feedback">
                    An ISBN is required.
                </div>
              </div>

              <div className="col-md-8 mb-3">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" defaultValue="" required />
                <div className="invalid-feedback">
                    A book title is required.
                </div>
              </div>

              <div className="col-md-2 mb-3">
                <label htmlFor="pages">Pages</label>
                <input type="number" className="form-control" id="pages" defaultValue="100" required />
                <div className="invalid-feedback">
                    The number of pages is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add book</button>
          </form>
        </div>
      </div>

      <table className="table">
        <thead>
          <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Pages</th>
          </tr>
        </thead>
        <tbody>
            {this.state.books && this.state.books.map(book => <tr key={book._id}>
            <td>{book.isbn}</td>
            <td>{book.title}</td>
            <td>{book.pages}</td>
            <td><button onClick={this.deleteBook.bind(this, book._id)} type="button" className="btn btn-danger">Delete</button></td>
          </tr>)}

        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Books;
