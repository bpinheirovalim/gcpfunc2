import feathers from '@feathersjs/client';
import rest from '@feathersjs/rest-client';

const client = feathers();

// YOU HAVE TO UPDATE THIS WITH YOUR OWN DOMAIN NAME
const restClient = rest('https://us-central1-cedar-lexicon-276218.cloudfunctions.net/feathers');

client.configure(restClient.fetch(window.fetch));

export default client;
